

function carregarAgenda() {
  console.log('executando Ajax..');
  var result = [];
  $.ajax({				
    url: CI_ROOT +'ConselhoAdmin/reunioesAdminJSON',
    dataType: 'json',
    method: "POST",   
  })
  .done(function(retorno) {
    result = retorno;    
    console.log('resultado: '+ result);
    return result;
  });
  
  
};


  
/*
window.onload = function(){
  console.log('onLoad');
  eventsInline = carregarAgenda();
  console.log('buscou dados');
}*/

$(function(){
   
  
  var eventsInline = [];
  
  /* var carregarAgenda = function(){
    var result = [];
    $.ajax({				
      url: CI_ROOT +'ConselhoAdmin/reunioesAdminJSON',
      dataType: 'json',
      method: "POST",   
    })
    .done(function(retorno) {
      result = retorno;
      console.log('result:'+ result);
    });
    return result;
  };*/
      
    
  
    //var teste =  carregarAgenda(); 
   //console.log('teste: ' + eventsInline);
  
    
  /* initialize the calendar
    -----------------------------------------------------------------*/

  
  var currentLangCode = 'pt-br';	
  var currentTimezone = 'America/Sao_Paulo';    

  var _calendarInstance = $('#calendar').fullCalendar({
    header: {
      left: 'prev,next today',
      center: 'title',
      right: 'month,agendaWeek,agendaDay'
    },
    buttonText: {
      today: 'hoje',
      month: 'mês',
      week: 'semana',
      day: 'dia'
    },
    //Random default events
    events: function(start, end, timezone, callback) {
      $.ajax({
          url: CI_ROOT +'ConselhoAdmin/reunioesAdminJSON',
          dataType: 'json',
          method: "POST",         
          success: function(doc) {
              var events = doc;             
              callback(events);
          }
      });
    },
    lang: currentLangCode,
    timezone: currentTimezone,
    buttonIcons: false, // show the prev/next text
    weekNumbers: false,
    eventLimit: true, // allow "more" link when too many events
    selectable: true,
    timeFormat: 'H:mm',
    eventDurationEditable:true,
    editable: true,
    droppable: true, // this allows things to be dropped onto the calendar !!!

    eventClick: function(calEvent, jsEvent, view) {
      
              console.log(calEvent);

              BootstrapDialog.show({
                type: 			BootstrapDialog.TYPE_PRIMARY,
                size:       BootstrapDialog.SIZE_WIDE,
                title: 			'<h3><i class="fa fa-calendar"></i> Calendário de Reuniões</h3>',
                message: 		'<h4><i class="fa fa-users"></i> Conselho Admnistrativo <small>  '+calEvent.dataEvento+'</small></h4>' + 
    
                        '<hr />'+
                            
                        '<div class="form-group"><input  type="text" class="form-control" id="titulo" value="'+calEvent.title+'" /></div>' +                          
                        '<div class="form-group"><input  type="text" class="form-control" id="local" value="'+calEvent.local+'" /></div>' +                          
                        '<div class="form-group"><label>Hora Início</label><input  type="time" class="form-control" id="horaInicio" value="'+calEvent.horaInicio+'/></div>' +
                        '<div class="form-group"><label>Hora Fim</label><input  type="time" class="form-control" id="horaFim" value="'+calEvent.horaFim+'/></div>' +
                        '<div class="form-group"><textarea class="form-control" id="pauta" placeholder="Pauta" rows="3">'+calEvent.pauta+'</textarea></div>' +    
                        
    
                        '<hr />'+
    
                        /* start event color */
                        '<div class="form-group ">' +
                          '<label class="fsize11 block margin-top-20">Cor do Evento</label><br>' +
                          '<div class="radio radio-default radio-inline"><input type="radio" name="colorEvent" id="default" class="bg-default" value="#555555"><label for="default" ><span class="text-default">Cinza (Padrão)</span></label></div>'+                            
                          '<div class="radio radio-primary radio-inline"><input type="radio" name="colorEvent" id="primary" class="bg-primary" value="#0073b7"><label for="primary" ><span class="text-primary">Azul</span></label></div>'+                            
                          '<div class="radio radio-danger radio-inline"><input type="radio" name="colorEvent" id="danger" class="bg-danger" value="#f56954"><label for="danger" ><span class="text-danger">Vermelho</span></label></div>'+                            
                          '<div class="radio radio-warning radio-inline"><input type="radio" name="colorEvent" id="warning" class="bg-warning" value="#f39c12"><label for="warning" ><span class="text-warning">Amarelo</span></label></div>'+                            
                          '<div class="radio radio-success radio-inline"><input type="radio" name="colorEvent" id="success" class="bg-success" value="#00a65a"><label for="success" ><span class="text-success">Verde</span></label></div>'+                            
                          '<div class="radio radio-info radio-inline"><input type="radio" name="colorEvent" id="info" class="bg-info" value="#00c0ef"><label for="info" ><span class="text-info">Azul Claro</span></label></div>'+                            
                        '</div>' +
                        
                        /* end event color */
    
                        '',
                buttons: [                  
                  {
                    label: '<i class="fa fa-times"></i> Fechar',
                    cssClass: 'btn-danger',
                    action: function(dialogItself){
                      dialogItself.close();
                    }
                  }
                ]
              });

      
    },    
    dayClick: function(date, jsEvent, view) {	
          
          
          
          var dataEvento = date.format('DD/MM/YYYY');

          BootstrapDialog.show({
            type: 			BootstrapDialog.TYPE_PRIMARY,
            size:       BootstrapDialog.SIZE_WIDE,
            title: 			'<h3><i class="fa fa-calendar"></i> Criar Evento</h3>',
            message: 		'<h4><i class="fa fa-users"></i> Conselho Admnistrativo <small>  '+dataEvento+'</small></h4>' + 

                    '<hr />'+
                        
                    '<div class="form-group"><input required type="text" class="form-control" id="titulo" placeholder="Titulo do Evento *" /></div>' +                          
                    '<div class="form-group"><input required type="text" class="form-control" id="local" placeholder="Local" /></div>' +                          
                    '<div class="form-group"><label>Hora Início</label><input required type="time" class="form-control" id="horaInicio" /></div>' +
                    '<div class="form-group"><label>Hora Fim</label><input required type="time" class="form-control" id="horaFim" /></div>' +
                    '<div class="form-group"><textarea class="form-control" id="pauta" placeholder="Pauta" rows="3"></textarea></div>' +    
                    

                    '<hr />'+

                    /* start event color */
                    '<div class="form-group ">' +
                      '<label class="fsize11 block margin-top-20">Cor do Evento</label><br>' +
                      '<div class="radio radio-default radio-inline"><input type="radio" name="colorEvent" id="default" class="bg-default" value="#555555"><label for="default" ><span class="text-default">Cinza (Padrão)</span></label></div>'+                            
                      '<div class="radio radio-primary radio-inline"><input type="radio" name="colorEvent" id="primary" class="bg-primary" value="#0073b7"><label for="primary" ><span class="text-primary">Azul</span></label></div>'+                            
                      '<div class="radio radio-danger radio-inline"><input type="radio" name="colorEvent" id="danger" class="bg-danger" value="#f56954"><label for="danger" ><span class="text-danger">Vermelho</span></label></div>'+                            
                      '<div class="radio radio-warning radio-inline"><input type="radio" name="colorEvent" id="warning" class="bg-warning" value="#f39c12"><label for="warning" ><span class="text-warning">Amarelo</span></label></div>'+                            
                      '<div class="radio radio-success radio-inline"><input type="radio" name="colorEvent" id="success" class="bg-success" value="#00a65a"><label for="success" ><span class="text-success">Verde</span></label></div>'+                            
                      '<div class="radio radio-info radio-inline"><input type="radio" name="colorEvent" id="info" class="bg-info" value="#00c0ef"><label for="info" ><span class="text-info">Azul Claro</span></label></div>'+                            
                    '</div>' +
                    
                    /* end event color */

                    '',
            buttons: [
              {
                label: 		'<i class="fa fa-check"></i> Criar Evento',
                cssClass: 	'btn-success',
                hotkey: 	13, // Enter.
                action: function(dialogItself) {
                  $.ajax({				
                    url: CI_ROOT +'ConselhoAdmin/cadastrarReuniao',
                    dataType: 'json',
                    method: "POST",
                    data:{
                        titulo: $('#titulo').val(),
                        dataDoEvento: date.format('YYYY-MM-DD'),                              
                        horaInicio: $('#horaInicio').val(),
                        horaFim: $('#horaFim').val(),
                        pauta: $('#pauta').val(),
                        local: $('#local').val(),
                        bgColor: $("input:radio[name=colorEvent]:checked").val()
                    }            
                  })
                  .done(function(retorno){ 
                      console.log(retorno);  
                      location.reload();
                  })
                  .fail(function(jqXHR,textStatus) {
                      console.log("Request failed: " + textStatus);
                  });
                } 
              }, 
              {
                label: '<i class="fa fa-times"></i> Fechar',
                cssClass: 'btn-danger',
                action: function(dialogItself){
                  dialogItself.close();
                }
              }
            ]
          });

        }
    
  });
    

}); 