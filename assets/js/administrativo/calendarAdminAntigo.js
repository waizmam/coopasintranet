$(function () {
    
 
    
        /* initialize the calendar
         -----------------------------------------------------------------*/
        //Date for the calendar events (dummy data)
        var date = new Date();
        var d = date.getDate(),
            m = date.getMonth(),
            y = date.getFullYear();

            var currentLangCode = 'pt-br';	
            var currentTimezone = 'America/Sao_Paulo';
            //var eventsInline =<?php echo  $jsoncalendario?>;

        $('#calendar').fullCalendar({
          header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
          },
          buttonText: {
            today: 'hoje',
            month: 'mês',
            week: 'semana',
            day: 'dia'
          },
          //Random default events
          lang: currentLangCode,
					timezone: currentTimezone,
					buttonIcons: false, // show the prev/next text
          weekNumbers: false,
          eventLimit: true, // allow "more" link when too many events
					//events: eventsInline,
					selectable: true,
					timeFormat: 'H:mm',
					eventDurationEditable:true,
          editable: true,
          droppable: true, // this allows things to be dropped onto the calendar !!!
          drop: function (date, allDay) { // this function is called when something is dropped
    
            // retrieve the dropped element's stored Event Object
            var originalEventObject = $(this).data('eventObject');
    
            // we need to copy it, so that multiple events don't have a reference to the same object
            var copiedEventObject = $.extend({}, originalEventObject);
    
            // assign it the date that was reported
            copiedEventObject.start = date;
            copiedEventObject.allDay = allDay;
            copiedEventObject.backgroundColor = $(this).css("background-color");
            copiedEventObject.borderColor = $(this).css("border-color");
    
            // render the event on the calendar
            // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
            $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);
    
            // is the "remove after drop" checkbox checked?
            if ($('#drop-remove').is(':checked')) {
              // if so, remove the element from the "Draggable Events" list
              $(this).remove();
            }
    
          }
        });
    
       
});