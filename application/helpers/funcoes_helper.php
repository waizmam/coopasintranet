﻿<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
**  CodeIgniter
**   
**	FUNÇÕES DE TRATAMENTO E VALIDAÇÃO DE VARIÁVEIS
** 
**	autor: Elom Waizmam
*/

function retiraFrame($movie_file){
	
	//Dont' timeout
	set_time_limit(0);

	//Load the file (This can be any file - still takes ages) 
	$mov = new ffmpeg_movie($movie_file);
	$nome = $mov->getFilename(); 
	//Pega o total de quadros dentro da filme 
	$total_frames = $mov->getFrameCount();

	//Loop 5-10 vezes para gerar quadros aleatórios 5-10 vezes 
	for ($i = 1; $i <= 3; ) {
		// Gera um número de 200 a o número total de quadros.
		$frame = mt_rand(30,$total_frames);
		$getframe = $mov->getFrame($frame);
		
		// Verificar se o quadro existe dentro do filme 
		// Se isso acontecer, coloque o número do quadro dentro de uma matriz e quebrar o ciclo atual
		if($getframe){
		  $frames[$frame] = $getframe ;
		  $i++;
		}
	}
	
	$nomesImagens = array();
	
	//For each frame found generate a thumbnail
	foreach ($frames as $key => $getframe) {
		$gd_image = $getframe->toGDImage();
		imagejpeg($gd_image, '_upload/videos/img' .$key.'-'.$nome.'.jpeg',100);
		imagedestroy($gd_image);
		$nomesImagens = $key.'-'.$nome.'.jpg<br/>';
 
	}


}

/*
**  Função para ordenar array multidimensional ou merge de arrays
*/
function comparar_objetos($obj1, $obj2) {
    $anterior = $obj1->idPauta + $obj2->idPauta;
    if ($anterior != 0) {
        return $anterior;
    }

    $anterior = $obj1->id - $obj2->id;
    return $anterior;
}


/*
**  Converte a Data Para ser inserida no Banco de Dados
*/
function converteDataBanco($data) {

	if(strpos($data, '-')){
		$d = explode("-", $data);
		return trim($d[2]).'-'.trim($d[1]).'-'.trim($d[0]);
	} else if(strpos($data, '/')) {
		$d = explode("/", $data);
		return trim($d[2]).'-'.trim($d[1]).'-'.trim($d[0]);
	}
	
}


/*
**  Validando a Data Para ser inserida no Banco de Dados
*/
function ValidaData($data){
	
	// fatia a string $data em pedaços, usando - como referência
	if(strpos($data, '-')){
		$data = explode("-",$data); 
	} else if(strpos($data, '/')){
		$data = explode("/",$data);
	}
	$d = (int) $data[0];
	$m = (int) $data[1];
	$y = (int) $data[2];

	// verifica se a data é válida!
	// 1 = true (válida)
	// 0 = false (inválida)
	$res = checkdate($m,$d,$y);
	
	return $res;
}

/*
**  Verificando se a 1ªdata é menor que a 2ª
*/
function datasCoerentes($data1, $data2) {

        $inicio = explode("/",$data1);
        $i = mktime(0,0,0,$inicio[1],$inicio[2],$inicio[0]);
		//$i = intval( $inicio[1]) + intval($inicio[2]) + intval($inicio[0]);

        $termino = explode("/",$data2);
        $t = mktime(0,0,0,$termino[1],$termino[2],$termino[0]);
		//$t = intval($termino[1]) + intval($termino[2]) + intval($termino[0]);
		
		echo '<br>';
		echo $i . '<br>';
		echo $t . '<br>';
		echo '<br>';
		exit();
				
        if(($t - $i) < 0) {
            return false;
        }
		else if(($t - $i) == 0){
			return 2;
		}
        else{
        	return true;
		}
}

function validaHora($hora){
	
	if (preg_match('/^[0-9]{2}:[0-9]{2}$/', $hora)) {
		
	   list($hour,$minute) = explode(':',$hora);
 
	   if ($hour > -1 && $hour < 24 && $minute > -1 && $minute < 60) {
	     	return true;
	   }
	   else{
	   	  	return false;
	   }
		
	} else {
		return false;
	}
	
}


function compararHora($hora){
	
	$horafixa = strtotime($hora);
	$horaatual = strtotime(date('H:i:s'));
 
	if($horaatual > $horafixa){
	    /*print("Agora a hora é maior\n");
	    print($horaatual);*/
	    return true;
	}else{
		return false;
	}
	
}


/*
**  Verificando se a 1ªdata é menor que a 2ª
*/
function CompararDatas($data1, $data2) {

        $data_inicial = implode(array_reverse(explode("/", $data1))); 
		
		$data_final = implode(array_reverse(explode("/", $data2))); 
		
		if($data_inicial > $data_final){
			return TRUE;
		}else{
			return FALSE;
		}
}


/*
**  Converte a Data Para ser mostrada nas views
*/
function converteDataInterface($data) {

	if(strpos($data, '-')){
		$d = explode("-", $data);
		return $d[2].'/'.$d[1].'/'.$d[0];
	} else if(strpos($data, '/')) {
		$d = explode("/", $data);
		return $d[2].'/'.$d[1].'/'.$d[0];
	}
	
}

/*
**  Dia da Semana - Agendamento
*/
function diasemana($data) {
	$ano =  substr("$data", 0, 4);
	$mes =  substr("$data", 5, -3);
	$dia =  substr("$data", 8, 9);

	$diasemana = date("w", mktime(0,0,0,$mes,$dia,$ano) );

	switch($diasemana) {
		case"0": $diasemana = "Domingo";       break;
		case"1": $diasemana = "Segunda-Feira"; break;
		case"2": $diasemana = "Terça-Feira";   break;
		case"3": $diasemana = "Quarta-Feira";  break;
		case"4": $diasemana = "Quinta-Feira";  break;
		case"5": $diasemana = "Sexta-Feira";   break;
		case"6": $diasemana = "Sábado";        break;
	}

	return "$diasemana";
}


/*
**  Trata o campos dos formulários
*/
function sql_inject($campo){
	$campo = get_magic_quotes_gpc() == 0 ? addslashes($campo) : $campo;
	$campo = strip_tags($campo);
	$campo = trim($campo);
	return preg_replace("@(--|\#|\*|;|=)@s", "", $campo); 
}


 
/*
**  Ajustando o CEP - tirando os ("." e "-") 
*/	
function tratarCEP($cep) {
	
	$array_cep = explode("-", $cep);
	$cep_tratado = $array_cep[0] . $array_cep[1];
	
	return $cep_tratado;
	
}


/*
** Ajustando o Telefone - tirando os ("()" e "-")
*/	
function tratarTelefone($telefone) {
	
	$array_telefone = explode("(", $telefone);
	$telefone1 = $array_telefone[0] . $array_telefone[1];
	
	$array_telefone2 = explode(")", $telefone1);
	$telefone2 = $array_telefone2[0] . $array_telefone2[1];
	
	$array_telefone = explode("-", $telefone2);
	$telefone_tratado = $array_telefone[0] . $array_telefone[1] ;
	
	return $telefone_tratado;
	
}


/*
** Validando Email
*/	
function validarEmail ($email) {
        $email=trim (strtolower($email));
        if (strlen($email)<6) return false;
        if (!preg_match('/^[a-z0-9]+([\._-][a-z0-9]+)*@[a-z0-9_-]+(\.[a-z0-9]+){0,4}\.[a-z0-9]{1,4}$/',$email)) return false;
        //$dominio=end(explode ('@',$email));
        //if (!gethostbynamel ($dominio)) return false;
        return true;
}

/*
** Tratando Valores Monetários
*/	
function moeda($get_valor) {

	$array_valor = explode("R$", $get_valor);
	$get_valor = $array_valor[0] . $array_valor[1];
	
	$source = array('.', ',');  
	$replace = array('', '.');  
	$valor = str_replace($source, $replace, $get_valor); //remove os pontos e substitui a virgula pelo ponto  
	return $valor; //retorna o valor formatado para gravar no banco  
} 



/* /*
**  Ajustando o CNPJ - tirando os ("." e "-") 
*/				
function tratarCNPJ($cnpj) {
			
	$array_cnpj = explode("-", $cnpj);
	$cnpj1 = @$array_cnpj[0] . @$array_cnpj[1];
			
	$array_cnpj2 = explode("/", $cnpj1);
	$cnpj2 = @$array_cnpj2[0] . @$array_cnpj2[1]. @$array_cnpj2[2];
	
	$array_cnpj3 = explode(".", $cnpj2);
	$cnpj_tratado = @$array_cnpj3[0] . @$array_cnpj3[1]. @$array_cnpj3[2];
	
	return $cnpj_tratado;
	
} 

/*
**  Ajustando o CPF - tirando os ("." e "-") 
*/				
function tratarCPF($cpf) {
			
	$array_cpf = explode("-", $cpf);
	$cpf1 = @$array_cpf[0] . @$array_cpf[1];
			
	$array_cpf2 = explode(".", $cpf1);
	$cpf_tratado = @$array_cpf2[0] . @$array_cpf2[1]. @$array_cpf2[2];
			
	return $cpf_tratado;
	
}



/*
**  Validando o CPF 
*/	
function validar_cpf($cpf) {

    // Verifiva se o número digitado contém todos os digitos
    $cpf = str_pad(preg_replace('/[^0-9]/', '', $cpf), 11, '0', STR_PAD_LEFT);
 
    // Verifica se nenhuma das sequências abaixo foi digitada, caso seja, retorna falso
    if (strlen($cpf) != 11 ||
        $cpf == '00000000000' ||
        $cpf == '11111111111' ||
        $cpf == '22222222222' ||
        $cpf == '33333333333' ||
        $cpf == '44444444444' ||
        $cpf == '55555555555' ||
        $cpf == '66666666666' ||
        $cpf == '77777777777' ||
        $cpf == '88888888888' ||
        $cpf == '99999999999') {
        return FALSE;
    } else {
        // Calcula os números para verificar se o CPF é verdadeiro
        for ($t = 9; $t < 11; $t++) {
            for ($d = 0, $c = 0; $c < $t; $c++) {
                $d += $cpf{$c} * (($t + 1) - $c);
            }
 
            $d = ((10 * $d) % 11) % 10;
            if ($cpf{$c} != $d) {
                return FALSE;
            }
        }
        return TRUE;
    }
}
 


/*
** Validando o CNPJ
*/	
function validaCNPJ($cnpj) { 
    if (strlen($cnpj) <> 18) return 0; 
    $soma1 = ($cnpj[0] * 5) + 

    ($cnpj[1] * 4) + 
    ($cnpj[3] * 3) + 
    ($cnpj[4] * 2) + 
    ($cnpj[5] * 9) + 
    ($cnpj[7] * 8) + 
    ($cnpj[8] * 7) + 
    ($cnpj[9] * 6) + 
    ($cnpj[11] * 5) + 
    ($cnpj[12] * 4) + 
    ($cnpj[13] * 3) + 
    ($cnpj[14] * 2); 
    $resto = $soma1 % 11; 
    $digito1 = $resto < 2 ? 0 : 11 - $resto; 
    $soma2 = ($cnpj[0] * 6) + 

    ($cnpj[1] * 5) + 
    ($cnpj[3] * 4) + 
    ($cnpj[4] * 3) + 
    ($cnpj[5] * 2) + 
    ($cnpj[7] * 9) + 
    ($cnpj[8] * 8) + 
    ($cnpj[9] * 7) + 
    ($cnpj[11] * 6) + 
    ($cnpj[12] * 5) + 
    ($cnpj[13] * 4) + 
    ($cnpj[14] * 3) + 
    ($cnpj[16] * 2); 
    $resto = $soma2 % 11; 
    $digito2 = $resto < 2 ? 0 : 11 - $resto; 
    return (($cnpj[16] == $digito1) && ($cnpj[17] == $digito2)); 
} 


/*
** Formatar 
*/
function formatar($string, $tipo = "")
{
    $string = preg_replace("[^0-9]", "", $string);
  
    if (!$tipo)
    {
        switch (strlen($string))
        {
        	
            case 10:    $tipo = 'fone';     break;
            case 8:     $tipo = 'cep';      break;
            case 11:    $tipo = 'cpf';      break;
            case 14:    $tipo = 'cnpj';     break;
        }

    }

    switch ($tipo)

    {
    	case 'celular':
            $string = '(' . substr($string, 0, 2) . ') ' . substr($string, 2, 5) . '-' . substr($string, 7);
        break;
		
        case 'fone':
            $string = '(' . substr($string, 0, 2) . ') ' . substr($string, 2, 4) . '-' . substr($string, 6);
        break;

        case 'cep':
            $string = substr($string, 0, 5) . '-' . substr($string, 5, 3);
        break;
		
        case 'cpf':
            $string = substr($string, 0, 3) . '.' . substr($string, 3, 3) . '.' . substr($string, 6, 3) . '-' . substr($string, 9, 2);
        break;

        case 'cnpj':
            $string = substr($string, 0, 2) . '.' . substr($string, 2, 3) . '.' . substr($string, 5, 3) . '/' . substr($string, 8, 4) . '-' . substr($string, 12, 2);
        break;
		
        case 'rg':
            $string = substr($string, 0, 2) . '.' . substr($string, 2, 3) . '.' . substr($string, 5, 3);
        break;
    }

    return $string;
	
	/*
	UTILIZAÇÃO
	echo formatar ('3135399000', 'fone');
	// (31) 3539-9000
	*/

}


/*
** URL Amigável (Friendly_url)
*/
function getRawUrl($url) {

	 
	$string = convert_accented_characters($url);	 
	
	$b =  url_title($string,TRUE);

	return strtolower($b);

}

/*
 *  Separar Data e hora de campos TIMESTAMP
 */
 
function separaDataHora($dataHora){
	
	$array = explode(' ', $dataHora);
	
	$data = $array[0];
	$hora = $array[1];
	
	$array_data = explode("-", $data);

	$ano = $array_data[0];
	$mes = $array_data[1];
	$dia = $array_data[2];
		
	$res = checkdate($mes,$dia,$ano);
	
	if ($res == 1){
		$data_valida = $dia .'/' .$mes .'/'. $ano;
		return $data_valida;	   
	}	
	
} 

function formatarDataHora($dataHora){
	
	$array = explode(' ', $dataHora);
	
	$data = $array[0];
	$hora = $array[1];
	
	$array_data = explode("-", $data);

	$ano = $array_data[0];
	$mes = $array_data[1];
	$dia = $array_data[2];
		
	$res = checkdate($mes,$dia,$ano);
	
	if ($res == 1){
		$data_valida = $dia .'/' .$mes .'/'. $ano;
		return $data_valida.' - '. $hora ;	   
	}	
	
} 

function retornaDataEstilizada($dataHora,$rcs = 0){ 
	
	$array = explode(' ', $dataHora);
	
	$hora = $array[1];	
	$tm = strtotime($dataHora);
		
	$cur_tm = time(); 
	$dif = $cur_tm-$tm;
    $pds = array('segundo','minuto','hora','dia','semana','mes','ano','decada');
    $lngh = array(1,60,3600,86400,604800,2630880,31570560,315705600);
    for($v = sizeof($lngh)-1; ($v >= 0)&&(($no = $dif/$lngh[$v])<=1); $v--); if($v < 0) $v = 0; $_tm = $cur_tm-($dif%$lngh[$v]);
   
    $no = floor($no); 
    
    if($no <> 1){ 
    	if ( $v == 5 )
    		$pds[$v] .='es atrás';
    	else
    		$pds[$v] .='s atrás'; 	 
    }else{ 
    	$pds[$v] .= ' atrás';
    }	 
    
    $x=sprintf("%d %s",$no,$pds[$v]);
    if(($rcs == 1)&&($v >= 1)&&(($cur_tm-$_tm) > 0)) $x .= time_ago($_tm);
    return $x;
	
}


function ip() {
		if     (getenv('HTTP_CLIENT_IP')){
			 $ip = getenv('HTTP_CLIENT_IP');      
		} 
		elseif (getenv('HTTP_X_FORWARDED_FOR')) {
			 $ip = getenv('HTTP_X_FORWARDED_FOR'); 
		} 
		elseif (getenv('HTTP_X_FORWARDED')){
			 $ip = getenv('HTTP_X_FORWARDED');     
		} 
		elseif (getenv('HTTP_FORWARDED_FOR')) {
			 $ip = getenv('HTTP_FORWARDED_FOR');   
		} 
		elseif (getenv('HTTP_FORWARDED')) {
			 $ip = getenv('HTTP_FORWARDED');       
		} 
		else {
			 $ip = $_SERVER['REMOTE_ADDR'];        
		} 
		return $ip;
}

function convert_object_to_array($data) {

    if (is_object($data)) {
        $data = get_object_vars($data);
    }

    if (is_array($data)) {
        return array_map(__FUNCTION__, $data);
    }
    else {
        return $data;
    }
}

function arrayToObject($array) {
    if (!is_array($array)) {
        return $array;
    }

    $object = new stdClass();
    if (is_array($array) && count($array) > 0) {
        foreach ($array as $name=>$value) {
            $name = strtolower(trim($name));
            if (!empty($name)) {
                $object->$name = arrayToObject($value);
            }
        }
        return $object;
    }
    else {
        return FALSE;
    }
}





