<?php

Class AdminDao_model extends CI_Model {

	public function __construct() {
		parent:: __construct();
	}
	
	
	function make_query_atas(){  
		$order_column = array("data","tags","resumo", "dataCadastro", "nome",null,null);  
		//$this->db->group_by('data');
		$this->db->select('idAtasAdmin,data,tags,resumo,arquivo,dataCadastro,usuario_responsavel,nome');  
		$this->db->from('tbl_atasAdmin');
		$this->db->join('tbl_usuario','tbl_usuario.idUsuario = tbl_atasAdmin.usuario_responsavel');
		if(isset($_POST["search"]["value"])){  
			if(!empty($_POST['columns'][1]["search"]["value"])){
				$this->db->like("data", $_POST['columns'][1]["search"]["value"]);
			}
			if(!empty($_POST['columns'][2]["search"]["value"])){
				$this->db->or_like("tags", $_POST['columns'][2]["search"]["value"]);
			}
			if(!empty($_POST['columns'][3]["search"]["value"])){
				$this->db->or_like("resumo", $_POST['columns'][3]["search"]["value"]); 
			}
			if(!empty($_POST['columns'][4]["search"]["value"])){
				$this->db->or_like("dataCadastro", $_POST['columns'][4]["search"]["value"]); 
			}
			if(!empty($_POST['columns'][5]["search"]["value"])){
				$this->db->or_like("nome", $_POST['columns'][5]["search"]["value"]); 
			}
			  
			
			
			
			
		}  
		if(isset($_POST["order"])){  
			$this->db->order_by($order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
		}  
		else{  
			$this->db->order_by('idAtasAdmin', 'DESC');  
		}  
	}

	function make_datatables_atas(){  
		$this->make_query_atas();  
		if($_POST["length"] != -1){  
			$this->db->limit($_POST['length'], $_POST['start']);  
		}  
		$query = $this->db->get();  
		return $query->result();  
    } 

	function get_filtered_data_atas(){  
		$this->make_query_atas();  
		$query = $this->db->get();  
		return $query->num_rows();  
    }

	function get_all_data_atas(){  
		$this->db->select("*");  
		$this->db->from('tbl_atasAdmin');  
		return $this->db->count_all_results();  
    }
	
	function insertReuniao($data){
		return $this->db->insert('tbl_reuniaoAdmin',$data);
		
	}

	function selectAgendaAdmin(){
		return $this->db->get('tbl_reuniaoAdmin')->result();
	}
	
	
}
?>