<?php

Class LoginDao_model extends CI_Model {

	public function __construct() {
		parent:: __construct();
	}
	
	function loginUser($data){
		$this->db->where('status','ATIVO');		
		$this->db->where('email',$data['email']);
		$this->db->where('senha',$data['senha']);
		$this->db->group_by('tbl_usuario.idUsuario,formacao');
		$this->db->select('tbl_usuario.idUsuario,nome,email,formacao,foto,avatar,GROUP_CONCAT(tbl_usuario_grupo.idGrupo),tipoUsuario');
		$this->db->from('tbl_usuario');
		$this->db->join('tbl_formacao','tbl_formacao.usuario_id = tbl_usuario.idUsuario','inner');
		$this->db->join('tbl_usuario_grupo','tbl_usuario_grupo.idUsuario = tbl_usuario.idUsuario','inner');
		return $this->db->get()->result();		
	}
	
		
	
}
?>