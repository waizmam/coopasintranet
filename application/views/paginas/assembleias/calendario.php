
<div class="wrapper">



  <?php $this->load->view('include/header');?>
  <?php $this->load->view('include/menuLateral');?>

   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
     <h1>
       Assembléias Coopas
       <small>Calendário </small>
     </h1>
     <ol class="breadcrumb">
       <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
       <li class="active">Calendário</li>
     </ol>
   </section>

   <!-- Main content -->
   <section class="content">
     <div class="row">
       
       <div class="col-md-12">
         <div class="box box-primary">
           <div class="box-body no-padding">
             <!-- THE CALENDAR -->
             <div id="calendar"></div>
           </div>
           <!-- /.box-body -->
         </div>
         <!-- /. box -->
       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->
   </section>
   <!-- /.content -->
 </div>
 <!-- /.content-wrapper -->
