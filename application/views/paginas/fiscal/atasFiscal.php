
<div class="wrapper">



  <?php $this->load->view('include/header');?>
  <?php $this->load->view('include/menuLateral');?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Conselho Fiscal
        <small>Atas de reuniões</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Conselho Fiscal - Atas</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">

          <div class="box">
              <div class="box-header">
                <h3 class="box-title">Atas do conselho</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Data da Reunião</th>
                    <th>Tags</th>
                    <th>Resumo</th>
                    <th>Data de Cadastro</th>
                    <th>Membro</th>
                    <th>Arquivo</th>
                  </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>06/04/2017</td>
                      <td>INSS,</td>
                      <td>AdminLTE can be installed using multiple methods. Pick your favorite method from the list below. Please be sure to check the dependencies section before continuing.</td>
                      <td>08/04/2017 - 10:45:00</td>
                      <td>Luciana Paiva</td>
                      <td><img src="<?php echo base_url() ?>assets/img/pdf.png"  class="img-thumbnail" width="70" height="110"></td>
                    </tr>
                    <tr>
                      <td>06/08/2017</td>
                      <td>HESPÉRIA,LICITAÇÃO</td>
                      <td>AdminLTE can be installed using multiple methods. Pick your favorite method from the list below. Please be sure to check the dependencies section before continuing.</td>
                      <td>15/08/2017 - 10:45:00</td>
                      <td>Janderson</td>
                      <td><img src="<?php echo base_url() ?>assets/img/pdf.png"  class="img-thumbnail" width="70" height="110"></td>
                    </tr>
                    <tr>  
                      <td>12/08/2017</td>
                      <td>ADVERTÊNCIA,BÔNUS SOCIAL</td>
                      <td>AdminLTE can be installed using multiple methods. Pick your favorite method from the list below. Please be sure to check the dependencies section before continuing.</td>
                      <td>20/08/2017 - 10:45:00</td>
                      <td>Diogo</td>
                      <td><img src="<?php echo base_url() ?>assets/img/pdf.png"  class="img-thumbnail" width="70" height="110"></td>
                    </tr>
                  </tbody>
                    <tfoot>
                      <tr>
                        <th>Data da Reunião</th>
                        <th>Tags</th>
                        <th>Resumo</th>
                        <th>Data de Cadastro</th>
                        <th>2º Secretário</th>
                        <th>Arquivo</th>
                      </tr>
                  </tfoot>
                </table>
              </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->
          </div>
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
