
<div class="wrapper">

  <?php $this->load->view('include/header');?>
  <?php $this->load->view('include/menuLateral');?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dados Pessoais
        <small>Benefícios</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Dados Pessoais</a></li>
        <li class="active">Beneficios</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-4">

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
              <img class="profile-user-img img-responsive img-circle" src="<?php echo base_url() ?>assets/dist/img/user2-160x160.jpg" alt="User profile picture">

              <h3 class="profile-username text-center">Elom Waizmam</h3>

              <p class="text-muted text-center">Software Engineer</p>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>E-mail</b> <a class="pull-right">waizmam.rj@gmail.com</a>
                </li>
                <li class="list-group-item">
                  <b>Celular</b> <a class="pull-right">(21) 9999-9999</a>
                </li>

              </ul>

              <!--<a href="#" class="btn btn-primary btn-block"><b>Follow</b></a>-->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <!-- About Me Box -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Sobre</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <strong><i class="fa fa-book margin-r-5"></i> Formação Acadêmica</strong>

              <p class="text-muted">
                Engenharia de Software com Java - Instituto Infnet
              </p>
              <p class="text-muted">
                Análise e Desenvolvimento de Sistemas - Universidade Estácio de Sá
              </p>
              <p class="text-muted">
                Técnico em Processamento de Dados - Escola Técnica Estadual República - FAETEC
              </p>

              <hr>

              <strong><i class="fa fa-map-marker margin-r-5"></i> Endereço</strong>

              <p class="text-muted">Brasil, Rio de Janeiro</p>

              <hr>

              <strong><i class="fa fa-pencil margin-r-5"></i> Conhecimentos Técnicos</strong>

              <p>
                <span class="label label-danger">Banco de Dados</span>
                <span class="label label-success">Engenharia de Software</span>
                <span class="label label-info">Análise de Sistemas</span>
                <span class="label label-warning">PHP</span>
                <span class="label label-warning">JAVA</span>
                <span class="label label-warning">Web Design</span>
                <span class="label label-primary">Node.js</span>
              </p>

              <hr>

              <strong><i class="fa fa-file-text-o margin-r-5"></i> Notes</strong>

              <p>Pós-graduado em Engenharia de Software com Java pelo Instituto Infnet, Analista de Sistemas,
                graduado em Desenvolvimento e Análise de Sistemas pela Estácio de Sá, atuo a mais de 6 anos com
                desenvolvimento de sistemas para internet usando a Linguagem de servidor
                 PHP, frameworks CodeIgniter, Zend e Symfony, banco de Dados Mysql, Postgree, XHTML, HTML5, CSS, CSS 3, AJAX e JQuery.</p>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-8">

              <!-- SELECT2 EXAMPLE -->
              <div class="box box-warning">
                <div class="box-header with-border">
                  <h3 class="box-title">Benefícios</h3>

                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">

                <div class="info-box bg-red">
                  <span class="info-box-icon"><i class="ion ion-ios-heart-outline"></i></span>

                  <div class="info-box-content">
                    <span class="info-box-text">Assistência Médica</span>
                    <span class="info-box-number">R$ 187,90</span>

                    <div class="progress">
                      <div class="progress-bar" style="width: 20%"></div>
                    </div>
                    <span class="progress-description">
                          2 Dependentes, UNIMED DELTA
                        </span>
                  </div>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
                
                <div class="info-box bg-green">
                  <span class="info-box-icon"><i class="ion ion-android-contact"></i></span>

                  <div class="info-box-content">
                    <span class="info-box-text">Assistência Odontológica</span>
                    <span class="info-box-number">R$ 0,00</span>

                    <div class="progress">
                      <div class="progress-bar" style="width: 20%"></div>
                    </div>
                    <span class="progress-description">
                          0 Dependentes, UNIODONTO
                        </span>
                  </div>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
                

                <div class="info-box bg-yellow">
                  <span class="info-box-icon"><i class="ion ion-coffee"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">Vale Refeição</span>
                    <span class="info-box-number">R$ 550,00</span>

                    <div class="progress">
                      <div class="progress-bar" style="width: 70%"></div>
                    </div>
                    <span class="progress-description">
                         
                    </span>
                  </div>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->

                <div class="info-box bg-yellow">
                  <span class="info-box-icon"><i class="ion ion-android-cart"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">Vale Alimentação</span>
                    <span class="info-box-number">R$ 0,00</span>

                    <div class="progress">
                      <div class="progress-bar" style="width: 70%"></div>
                    </div>
                    <span class="progress-description">
                         
                    </span>
                  </div>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->

                <div class="info-box bg-blue">
                  <span class="info-box-icon"><i class="ion ion-android-bus"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">Auxílio Transporte</span>
                    <span class="info-box-number">R$ 230,00</span>

                    <div class="progress">
                      <div class="progress-bar" style="width: 70%"></div>
                    </div>
                    <span class="progress-description">
                         
                    </span>
                  </div>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->

            </div>
            <!-- /.box-body -->
        
        </div>
        <!-- /.box -->

          

        </div>
        <!-- /.col -->

      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->

  </div>
  <!-- /.content-wrapper -->
