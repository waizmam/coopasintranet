
<div class="wrapper">

  <?php $this->load->view('include/header');?>
  <?php $this->load->view('include/menuLateral');?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Meu Perfil
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Dados Pessoais</a></li>
        <li class="active">Meu Perfil</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-4">

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
              <img class="profile-user-img img-responsive img-circle" src="<?php echo base_url() ?>assets/dist/img/user2-160x160.jpg" alt="User profile picture">

              <h3 class="profile-username text-center">Alexander Pierce</h3>

              <p class="text-muted text-center">Software Engineer</p>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>E-mail</b> <a class="pull-right">waizmam.rj@gmail.com</a>
                </li>
                <li class="list-group-item">
                  <b>Celular</b> <a class="pull-right">(21) 9999-9999</a>
                </li>

              </ul>

              <!--<a href="#" class="btn btn-primary btn-block"><b>Follow</b></a>-->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <!-- About Me Box -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Sobre</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <strong><i class="fa fa-book margin-r-5"></i> Formação Acadêmica</strong>

              <p class="text-muted">
                Engenharia de Software com Java - Instituto Infnet
              </p>
              <p class="text-muted">
                Análise e Desenvolvimento de Sistemas - Universidade Estácio de Sá
              </p>
              <p class="text-muted">
                Técnico em Processamento de Dados - Escola Técnica Estadual República - FAETEC
              </p>

              <hr>

              <strong><i class="fa fa-map-marker margin-r-5"></i> Endereço</strong>

              <p class="text-muted">Brasil, Rio de Janeiro</p>

              <hr>

              <strong><i class="fa fa-pencil margin-r-5"></i> Conhecimentos Técnicos</strong>

              <p>
                <span class="label label-danger">Banco de Dados</span>
                <span class="label label-success">Engenharia de Software</span>
                <span class="label label-info">Análise de Sistemas</span>
                <span class="label label-warning">PHP</span>
                <span class="label label-warning">JAVA</span>
                <span class="label label-warning">Web Design</span>
                <span class="label label-primary">Node.js</span>
              </p>

              <hr>

              <strong><i class="fa fa-file-text-o margin-r-5"></i> Notes</strong>

              <p>Pós-graduado em Engenharia de Software com Java pelo Instituto Infnet, Analista de Sistemas,
                graduado em Desenvolvimento e Análise de Sistemas pela Estácio de Sá, atuo a mais de 6 anos com
                desenvolvimento de sistemas para internet usando a Linguagem de servidor
                 PHP, frameworks CodeIgniter, Zend e Symfony, banco de Dados Mysql, Postgree, XHTML, HTML5, CSS, CSS 3, AJAX e JQuery.</p>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-8">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#dadosPessoais" data-toggle="tab">Dados Pessoais</a></li>
              <li><a href="#endereco" data-toggle="tab">Endereço</a></li>
              <li><a href="#formacao" data-toggle="tab">Formação</a></li>
              <li><a href="#conhecimentoTecnico" data-toggle="tab">Conhecimentos Técnicos</a></li>
              <li><a href="#fotoAvatar" data-toggle="tab">Foto / Avatar</a></li>
            </ul>
            <div class="tab-content">
              <div class="active tab-pane" id="dadosPessoais">

                  <form action="<?php echo base_url()?>" method="post" class="form-horizontal">

                      <div class="form-group">
                        <label for="nomeCompleto" class="col-sm-2 control-label">Nome</label>
                        <div class="col-sm-10">
                          <input type="email" name="nomeCompleto" class="form-control" id="nomeCompleto" placeholder="Nome Completo">
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="email" class="col-sm-2 control-label">E-mail</label>
                        <div class="col-sm-10">
                          <input type="email" name="email" class="form-control" id="email" placeholder="E-mail">
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="senha" class="col-sm-2 control-label">Senha</label>
                        <div class="col-sm-10">
                          <input type="text" name="senha" class="form-control" id="senha" placeholder="Senha">
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="cpf" class="col-sm-2 control-label">CPF</label>
                        <div class="col-sm-10">
                        <input type="text" name="cpf" class="form-control" id="cpf" placeholder="CPF">
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="dataNascimento" class="col-sm-2 control-label">Data de Nascimento</label>
                        <div class="col-sm-10">
                          <input type="text" name="dataNascimento" class="form-control" id="dataNascimento" placeholder="Data de Nascimento">
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="descricao" class="col-sm-2 control-label">Descrição</label>
                        <div class="col-sm-10">
                          <textarea name="descricao" class="form-control" id="descricao" placeholder="Descrição"></textarea>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="telefoneFixo" class="col-sm-2 control-label">Telefone Fixo</label>
                        <div class="col-sm-10">
                          <input type="text" name="telefoneFixo" class="form-control" id="telefoneFixo" placeholder="Telefone Fixo">
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="telefoneCelular" class="col-sm-2 control-label">Telefone Celular</label>
                        <div class="col-sm-10">
                          <input type="text" name="telefoneCelular" class="form-control" id="telefoneCelular" placeholder="Telefone Celular">
                        </div>
                      </div>

                      
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-danger">Alterar</button>
                        </div>
                      </div>
                  </form>
                 
              </div>
              <!-- /.tab-pane -->

              <div class="tab-pane" id="endereco">                
                  <form action="<?php echo base_url()?>" method="post" class="form-horizontal">

                      <div class="form-group">
                        <label for="endereco" class="col-sm-2 control-label">Endereço</label>
                        <div class="col-sm-10">
                          <input type="text" name="endereco" class="form-control" id="endereco" placeholder="Endereço">
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="complemento" class="col-sm-2 control-label">Complemento</label>
                        <div class="col-sm-10">
                          <input type="text" name="complemento" class="form-control" id="complemento" placeholder="Complemento">
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="bairro" class="col-sm-2 control-label">Bairro</label>
                        <div class="col-sm-10">
                          <input type="text" name="bairro" class="form-control" id="bairro" placeholder="Bairro">
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="numero" class="col-sm-2 control-label">Número</label>
                        <div class="col-sm-10">
                          <input type="text" name="numero" class="form-control" id="numero" placeholder="Número">
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="cep" class="col-sm-2 control-label">CEP</label>
                        <div class="col-sm-10">
                          <input type="text" name="cep" class="form-control" id="cep" placeholder="CEP">
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="email" class="col-sm-2 control-label">UF</label>
                        <div class="col-sm-10">
                        <select name="uf" id="uf" class="form-control" style="width: 100%;">
                          <option value="SP">SP</option>
                          <option value="RJ">RJ</option>
                          <option value="MG">MG</option>
                        </select>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="cidade" class="col-sm-2 control-label">Cidade</label>
                        <div class="col-sm-10">
                        <select name="cidade" id="cidade" class="form-control" style="width: 100%;">
                          <option value="1">Rio de Janeiro</option>
                          <option value="2">Volta Redonda</option>
                          <option value="3">Angra dos Reis</option>
                        </select>
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-danger">Alterar</button>
                        </div>
                      </div>

                  </form>
              </div>
              <!-- /.tab-pane -->

              <div class="tab-pane" id="formacao">
                                
                  <form action="<?php echo base_url()?>" method="post" class="form-horizontal">

                      <div class="form-group">
                        <label for="instituicao" class="col-sm-2 control-label">Instituição</label>
                        <div class="col-sm-10">
                          <input type="text" name="instituicao" class="form-control" id="instituicao" placeholder="Instituição">
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="formacao" class="col-sm-2 control-label">Formação</label>
                        <div class="col-sm-10">
                          <input type="text" name="formacao" class="form-control" id="formacao" placeholder="Formação">
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="nivel" class="col-sm-2 control-label">Nível de Escolaridade</label>
                        <div class="col-sm-10">
                        <select name="nivel" id="nivel" class="form-control" style="width: 100%;">
                          <option value="SP">Nível Fundamental</option>
                          <option value="RJ">Nível Médio</option>
                          <option value="MG">Nível Superior</option>
                          <option value="MG">Pós-Graduado</option>
                          <option value="MG">Mestrado</option>
                          <option value="MG">Doutorado</option>
                          <option value="MG">Pós-Doutorado</option>
                        </select>
                        </div>
                      </div>

                  </form>

              </div>
              <!-- /.tab-pane -->

              <div class="tab-pane" id="conhecimentoTecnico">
                
                <form action="<?php echo base_url()?>" method="post" class="form-horizontal">
                  
                  <div class="form-group">
                    <label for="conhecimentoTecnico" class="col-sm-2 control-label">Conhecimento Técnico</label>
                    <div class="col-sm-10">
                      <input type="text" name="conhecimentoTecnico" class="form-control" id="conhecimentoTecnico" placeholder="Conhecimento Técnico">
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <button type="submit" class="btn btn-primary">Adicionar</button>
                    </div>
                  </div>

                </form>

              </div>
              <!-- /.tab-pane -->

              <div class="tab-pane" id="fotoAvatar">
                  
                    <ul id="listAvatar">                      
                      <?php for ($i=1; $i <=33 ; $i++) {                          
                          $avatar = 'avatar'.$i.'.png'; ?>
                          <li><img id="<?php echo $i ?>" src="<?php echo base_url().'assets/img/avatar/'.$avatar ?>" class="img-responsive img-thumbnail imgAvatar" title="<?php echo $avatar ?>"></li>                          
                        <?php } ?>
                    </ul>
                
              </div>
              <!-- /.tab-pane -->

            </div>
            <!-- /.tab-content -->

          </div>
          <!-- /.nav-tabs-custom -->

        </div>
        <!-- /.col -->

      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->

  </div>
  <!-- /.content-wrapper -->
