
<div class="wrapper">



  <?php $this->load->view('include/header');?>
  <?php $this->load->view('include/menuLateral');?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Vagas
        <small>Painel de Controle</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Vagas</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">

        <div class="col-xs-12">

          <div class="box">
          <div class="box-header">
            <h3 class="box-title">Vagas</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>Cargo</th>
                <th>Nível</th>
                <th>Experiência</th>
                <th>Salário</th>
                <th>Status</th>
              </tr>
              </thead>
              <tbody>
              <tr>
                <td>Almoxarife</td>
                <td>Médio</td>
                <td>não exige</td>
                <td>R$1.800,00</td>
                <td><span class="label label-success">ATIVO</spam></td>
              </tr>
              <tr>
                <td>Analista de Suporte</td>
                <td>Médio Técnico</td>
                <td>2 anos</td>
                <td>R$ 3.800,00</td>
                <td><span class="label label-danger">FECHADA</spam></td>
              </tr>
              <tr>
                <td>Repórter</td>
                <td>Superior</td>
                <td>3 anos</td>
                <td>R$ 4.800,00</td>
                <td><span class="label label-info">EM ANDAMENTO</spam></td>
              </tr>

              </tbody>
              <tfoot>
              <tr>
                <th>Cargo</th>
                <th>Nível</th>
                <th>Experiência</th>
                <th>Salário</th>
                <th>Status</th>
              </tr>
              </tfoot>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->



      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
