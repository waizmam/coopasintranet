
<div class="wrapper">



  <?php $this->load->view('include/header');?>
  <?php $this->load->view('include/menuLateral');?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Clientes
        <small>Painel de Controle</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Clientes</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">

          <div class="col-md-4">
            <!-- Widget: user widget style 1 -->
            <div class="box box-widget widget-user">
              <!-- Add the bg color to the header using any of the bg-* classes -->
              <div class="widget-user-header bg-black" style="background: url('<?php echo base_url() ?>assets/dist/img/clientes/fiocruz.jpg') center center;">
                <h3 class="widget-user-username">Fiocruz</h3>
                <h5 class="widget-user-desc">Fundação Oswaldo Cruz (Fiocruz), vinculada ao Ministério da Saúde</h5>
              </div>
              <div class="widget-user-image">
                <img class="img-circle" src="<?php echo base_url() ?>assets/dist/img/clientes/fiocruz_logo.jpg" alt="User Avatar">
              </div>
              <div class="box-footer">
                <div class="row">
                  <div class="col-sm-4 border-right">
                    <div class="description-block">
                      <h5 class="description-header">17</h5>
                      <span class="description-text">Projetos</span>
                    </div>
                    <!-- /.description-block -->
                  </div>
                  <!-- /.col -->
                  <div class="col-sm-4 border-right">
                    <div class="description-block">
                      <h5 class="description-header">5</h5>
                      <span class="description-text">Projetos em Andamento</span>
                    </div>
                    <!-- /.description-block -->
                  </div>
                  <!-- /.col -->
                  <div class="col-sm-4">
                    <div class="description-block">
                      <h5 class="description-header">12</h5>
                      <span class="description-text">Notas</span>
                    </div>
                    <!-- /.description-block -->
                  </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->
              </div>
            </div>
            <!-- /.widget-user -->
          </div>
          <!-- /.col -->

          <div class="col-md-4">
            <!-- Widget: user widget style 1 -->
            <div class="box box-widget widget-user">
              <!-- Add the bg color to the header using any of the bg-* classes -->
              <div class="widget-user-header bg-black" style="background: url('<?php echo base_url() ?>assets/dist/img/clientes/gravacao.jpg') center center;">
                <h3 class="widget-user-username">Futura</h3>
                <h5 class="widget-user-desc">É um canal de televisão educativo brasileiro</h5>
              </div>
              <div class="widget-user-image">
                <img class="img-circle" src="<?php echo base_url() ?>assets/dist/img/clientes/futura.png" alt="User Avatar">
              </div>
              <div class="box-footer">
                <div class="row">
                  <div class="col-sm-4 border-right">
                    <div class="description-block">
                      <h5 class="description-header">17</h5>
                      <span class="description-text">Projetos</span>
                    </div>
                    <!-- /.description-block -->
                  </div>
                  <!-- /.col -->
                  <div class="col-sm-4 border-right">
                    <div class="description-block">
                      <h5 class="description-header">5</h5>
                      <span class="description-text">Projetos em Andamento</span>
                    </div>
                    <!-- /.description-block -->
                  </div>
                  <!-- /.col -->
                  <div class="col-sm-4">
                    <div class="description-block">
                      <h5 class="description-header">12</h5>
                      <span class="description-text">Notas</span>
                    </div>
                    <!-- /.description-block -->
                  </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->
              </div>
            </div>
            <!-- /.widget-user -->
          </div>
          <!-- /.col -->

      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
