
<div class="wrapper">



  <?php $this->load->view('include/header');?>
  <?php $this->load->view('include/menuLateral');?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Conselho Administrativo
        <small>Membros do Conselho</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Conselho Administrativo - Membros</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">

        <div class="col-md-6">
            <!-- Widget: user widget style 1 -->
            <div class="box box-widget widget-user">
              <!-- Add the bg color to the header using any of the bg-* classes -->
              <div class="widget-user-header bg-aqua-active">
                <h3 class="widget-user-username">Alexander Pierce</h3>
                <h5 class="widget-user-desc">Presidente</h5>
              </div>
              <div class="widget-user-image">
                <img class="img-circle" src="<?php echo base_url() ?>assets/dist/img/user1-128x128.jpg" alt="User Avatar">
              </div>
              <div class="box-footer">
                <div class="row">                  
                  <div class="col-sm-6 border-right">
                    <div class="description-block">
                      <h5 class="description-header">alexander.pierce@coopas.tv</h5>
                      <span class="description-text">E-MAIL</span>
                    </div>
                    <!-- /.description-block -->
                  </div>
                  <!-- /.col -->
                  <div class="col-sm-6">
                    <div class="description-block">
                      <h5 class="description-header">99999-9999</h5>
                      <span class="description-text">Contato</span>
                    </div>
                    <!-- /.description-block -->
                  </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->
              </div>
            </div>
            <!-- /.widget-user -->
          </div>
          <!-- /.col -->

          <div class="col-md-6">
              <!-- Widget: user widget style 1 -->
              <div class="box box-widget widget-user">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-red-active">
                  <h3 class="widget-user-username">Norman Straus</h3>
                  <h5 class="widget-user-desc">Vice-Presidente</h5>
                </div>
                <div class="widget-user-image">
                  <img class="img-circle" src="<?php echo base_url() ?>assets/dist/img/user8-128x128.jpg" alt="User Avatar">
                </div>
                <div class="box-footer">
                  <div class="row">                    
                    <div class="col-sm-6 border-right">
                      <div class="description-block">
                        <h5 class="description-header">norman.straus@coopas.tv</h5>
                        <span class="description-text">E-MAIL</span>
                      </div>
                      <!-- /.description-block -->
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-6">
                      <div class="description-block">
                        <h5 class="description-header">99999-9999</h5>
                        <span class="description-text">Contato</span>
                      </div>
                      <!-- /.description-block -->
                    </div>
                    <!-- /.col -->
                  </div>
                  <!-- /.row -->
                </div>
              </div>
              <!-- /.widget-user -->
            </div>
            <!-- /.col -->

            <div class="col-md-4">
                <!-- Widget: user widget style 1 -->
                <div class="box box-widget widget-user">
                  <!-- Add the bg color to the header using any of the bg-* classes -->
                  <div class="widget-user-header bg-yellow-active">
                    <h3 class="widget-user-username">Jane</h3>
                    <h5 class="widget-user-desc">1º Secretário</h5>
                  </div>
                  <div class="widget-user-image">
                    <img class="img-circle" src="<?php echo base_url() ?>assets/dist/img/user7-128x128.jpg" alt="User Avatar">
                  </div>
                  <div class="box-footer">
                    <div class="row">
                      <div class="col-sm-6 border-right">
                        <div class="description-block">
                          <h5 class="description-header">jane@coopas.tv</h5>
                          <span class="description-text">E-MAIL</span>
                        </div>
                        <!-- /.description-block -->
                      </div>
                      <!-- /.col -->
                      <div class="col-sm-6">
                        <div class="description-block">
                          <h5 class="description-header">99999-9999</h5>
                          <span class="description-text">Contato</span>
                        </div>
                        <!-- /.description-block -->
                      </div>
                      <!-- /.col -->
                    </div>
                    <!-- /.row -->
                  </div>
                </div>
                <!-- /.widget-user -->
              </div>
              <!-- /.col -->

              <div class="col-md-4">
                  <!-- Widget: user widget style 1 -->
                  <div class="box box-widget widget-user">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-green-active">
                      <h3 class="widget-user-username">Nora</h3>
                      <h5 class="widget-user-desc">2º Secretário</h5>
                    </div>
                    <div class="widget-user-image">
                      <img class="img-circle" src="<?php echo base_url() ?>assets/dist/img/user4-128x128.jpg" alt="User Avatar">
                    </div>
                    <div class="box-footer">
                      <div class="row">
                        <div class="col-sm-6 border-right">
                          <div class="description-block">
                            <h5 class="description-header">nora@coopas.tv</h5>
                            <span class="description-text">E-MAIL</span>
                          </div>
                          <!-- /.description-block -->
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-6">
                          <div class="description-block">
                            <h5 class="description-header">99999-9999</h5>
                            <span class="description-text">Contato</span>
                          </div>
                          <!-- /.description-block -->
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->
                    </div>
                  </div>
                  <!-- /.widget-user -->
                </div>
                <!-- /.col -->

                <div class="col-md-4">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user">
                      <!-- Add the bg color to the header using any of the bg-* classes -->
                      <div class="widget-user-header bg-gray-active">
                        <h3 class="widget-user-username">John</h3>
                        <h5 class="widget-user-desc">3º Secretário</h5>
                      </div>
                      <div class="widget-user-image">
                        <img class="img-circle" src="<?php echo base_url() ?>assets/dist/img/user6-128x128.jpg" alt="User Avatar">
                      </div>
                      <div class="box-footer">
                        <div class="row">
                          <div class="col-sm-6 border-right">
                            <div class="description-block">
                              <h5 class="description-header">nora@coopas.tv</h5>
                              <span class="description-text">E-MAIL</span>
                            </div>
                            <!-- /.description-block -->
                          </div>
                          <!-- /.col -->
                          <div class="col-sm-6">
                            <div class="description-block">
                              <h5 class="description-header">99999-9999</h5>
                              <span class="description-text">Contato</span>
                            </div>
                            <!-- /.description-block -->
                          </div>
                          <!-- /.col -->
                        </div>
                        <!-- /.row -->
                      </div>
                    </div>
                    <!-- /.widget-user -->
                  </div>
                  <!-- /.col -->



      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
