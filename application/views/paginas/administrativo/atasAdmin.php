
<div class="wrapper">



  <?php $this->load->view('include/header');?>
  <?php $this->load->view('include/menuLateral');?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Conselho Administrativo
        <small>Atas de reuniões</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Conselho Administrativo - Atas</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">

          <div class="box">
              <div class="box-header">
                <h3 class="box-title">Atas do conselho</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">

                <div class="box box-default box-solid">
                  <div class="box-header with-border">
                    <h3 class="box-title">Funções</h3>                   
                  </div>
                  <div class="box-body">                    
                    <div class="row">
                      <div class="col-lg-4">
                        <a href="<?php echo base_url('ConselhoAdmin/viewCadastroAta')?>" class="btn btn-primary"><i class="fa fa-plus"></i> Cadastrar Ata</a>                        
                      </div><!-- /.col-lg-6 -->                      
                    </div><!-- /.row -->                                                         
                 </div>
                </div>

                <table id="listaAtas" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Data da Reunião</th>
                    <th>Tags</th>
                    <th>Resumo</th>
                    <th>Data de Cadastro</th>
                    <th>2º Secretário</th>
                    <th>Arquivo</th>
                    <th>Ação</th>
                  </tr>
                  </thead>
                  <thead>
                    <tr class="busca">
                      <th><input type="text" data-column="1" value=""  class="form-control pull-right search-input-data" id="datepicker1" placeholder="Data ..." style="width:100%;"></th>
                      <th><input type="text" data-column="2" class="form-control search-input-text"  placeholder="pesquisar Tags ..." style="width:100%;"></th>
                      <th><input type="text" data-column="3" class="form-control search-input-text"  placeholder="pesquisar Resumo ..." style="width:100%;"></th>
                      <th><input type="text" data-column="4" value=""  class="form-control pull-right search-input-data" id="datepicker2" placeholder="Data ..." style="width:100%;"></th>                    
                      <th><input type="text" data-column="5" class="form-control search-input-text"  placeholder="pesquisar Secretário ..." style="width:100%;"></th>
                      <th></th>                    
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    
                  </tbody>
                    <tfoot>
                      <tr>
                        <th>Data da Reunião</th>
                        <th>Tags</th>
                        <th>Resumo</th>
                        <th>Data de Cadastro</th>
                        <th>2º Secretário</th>
                        <th>Arquivo</th>
                        <th>Ação</th>
                      </tr>
                  </tfoot>
                </table>

              </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->
          </div>
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
