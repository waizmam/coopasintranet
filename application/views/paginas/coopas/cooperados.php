
<div class="wrapper">



  <?php $this->load->view('include/header');?>
  <?php $this->load->view('include/menuLateral');?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Coopas
        <small>Cooperados</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Coopas</a></li>
        <li class="active">Cooperados</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">

        <div class="col-md-4">
          <!-- Widget: user widget style 1 -->
          <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-yellow">
              <div class="widget-user-image">
                <img class="img-circle" src="<?php echo base_url() .'assets/img/avatar/avatar5.png'?>" alt="User Avatar">
              </div>
              <!-- /.widget-user-image -->
              <h3 class="widget-user-username">Elom Waizmam</h3>
              <h5 class="widget-user-desc">Analista de Sistemas</h5>
            </div>
            <div class="box-footer no-padding">
              <ul class="nav nav-stacked">
                <li><a href="#">Telefone <span class="pull-right label  bg-blue">(21) 2222-2222</span></a></li>
                <li><a href="#">Celular <span class="pull-right label  bg-green">(21) 98888-8888</span></a></li>
                <li><a href="#">Email<span class="pull-right label  bg-aqua-active">elom.carvalho@gmail.com</span></a></li>
              </ul>
            </div>
          </div>
          <!-- /.widget-user -->
        </div>
        <!-- /.col -->

        <div class="col-md-4">
          <!-- Widget: user widget style 1 -->
          <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-aqua-active">
              <div class="widget-user-image">
                <img class="img-circle" src="<?php echo base_url() .'assets/img/avatar/avatar1.png'?>" alt="User Avatar">
              </div>
              <!-- /.widget-user-image -->
              <h3 class="widget-user-username">Antônio Carlos</h3>
              <h5 class="widget-user-desc">Administrador de Redes</h5>
            </div>
            <div class="box-footer no-padding">
              <ul class="nav nav-stacked">
                <li><a href="#">Telefone <span class="pull-right label  bg-blue">(21) 2222-2222</span></a></li>
                <li><a href="#">Celular <span class="pull-right label  bg-green">(21) 98888-8888</span></a></li>
                <li><a href="#">Email<span class="pull-right label  bg-aqua-active">carlosac.rd@gmail.com</span></a></li>
              </ul>
            </div>
          </div>
          <!-- /.widget-user -->
        </div>
        <!-- /.col -->

        <div class="col-md-4">
          <!-- Widget: user widget style 1 -->
          <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-green">
              <div class="widget-user-image">
                <img class="img-circle" src="<?php echo base_url() .'assets/img/avatar/avatar1.png'?>" alt="User Avatar">
              </div>
              <!-- /.widget-user-image -->
              <h3 class="widget-user-username">Manoel Ruis</h3>
              <h5 class="widget-user-desc">Analista de Suporte</h5>
            </div>
            <div class="box-footer no-padding">
              <ul class="nav nav-stacked">
                <li><a href="#">Telefone <span class="pull-right label  bg-blue">(21) 2222-2222</span></a></li>
                <li><a href="#">Celular <span class="pull-right label  bg-green">(21) 98888-8888</span></a></li>
                <li><a href="#">Email<span class="pull-right label  bg-aqua-active">ruis.manoel@gmail.com</span></a></li>
              </ul>
            </div>
          </div>
          <!-- /.widget-user -->
        </div>
        <!-- /.col -->


      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
