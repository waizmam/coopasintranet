
<div class="wrapper">



  <?php $this->load->view('include/header');?>
  <?php $this->load->view('include/menuLateral');?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Coopas
        <small>Colaboradores</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Coopas</a></li>
        <li class="active">Colaboradores</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">

        <div class="col-md-4">
          <!-- Widget: user widget style 1 -->
          <div class="box box-widget widget-user">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-green">
              <h3 class="widget-user-username">Renata Lima</h3>
              <h5 class="widget-user-desc">Contadora</h5>
            </div>
            <div class="widget-user-image">
              <img class="img-circle" src="<?php echo base_url() .'assets/img/avatar/avatar2.png'?>" alt="User Avatar">
            </div>
            <div class="box-footer">

              <div class="row">
                <div class="col-sm-6">
                  <div class="description-block ">
                    <h5 class="description-header">Telefones</h5>
                    <span class="description-text">(21) 99999-9999 </span>
                  </div>
                  <!-- /.description-block -->
                </div>
              </div>
              <!-- /.row -->  

              <div class="row">
                <!-- /.col -->
                <div class="col-sm-6">
                  <div class="description-block">
                    <h5 class="description-header">Email</h5>
                    <span class="description-text">elom.carvalho@gmail.com</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->            

            </div>
          </div>
          <!-- /.widget-user -->
        </div>
        <!-- /.col -->

        

      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
