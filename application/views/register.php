
<body class="hold-transition register-page">
<div class="register-box">
  <div class="register-logo">
    <a href="../../index2.html"><b>COOPAS</b>ADMIN</a>
  </div>

  <div class="register-box-body">
    <?php $this->load->view('include/alertsMsg') ?>
    <p class="login-box-msg">Informe seus dados para cadastro </p>

    <form action="<?php echo base_url('LoginIntranet/cadatrarRegistro') ?>" method="post">
      <div class="form-group has-feedback">
        <input name="nomeCompleto" type="text" class="form-control" placeholder="Nome Completo" required>
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input name="email" type="email" class="form-control" placeholder="E-mail" required>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input name="senha" type="password" class="form-control" placeholder="Senha" required>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input name="cpf" type="text" class="form-control" placeholder="CPF" data-inputmask='"mask": "999.999.999-99"' data-mask required>
        <span class="glyphicon glyphicon-info-sign form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input name="dataNascimento" type="text" class="form-control" placeholder="Data de Nascimento" data-inputmask='"mask": "99/99/9999"' data-mask required>
        <span class="glyphicon glyphicon-calendar form-control-feedback"></span>
      </div>
      <div class="form-group">      
        <label for="tipoUsuario" class="control-label">Tipo</label>
        <div>
          <select name="tipoUsuario" id="tipoUsuario" class="form-control" style="width: 100%;" required>
            <option value="">selecione --></option>
            <option value="COO">COOPERADO</option>
            <option value="COL">COLABORADOR</option>
          </select>
        </div>                      
      </div>
      
      <div class="row">
        <div class="col-xs-8">
          
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Cadastrar</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

  </div>
  <!-- /.form-box -->
</div>
<!-- /.register-box -->

