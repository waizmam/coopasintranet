<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <!--<img src="<?php echo base_url() ?>assets/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">-->
        <img src="<?php echo base_url() .'assets/img/avatar/avatar5.png'?>" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
          <?php //$nome = explode(' ' ,$this->session->userdata("nomeUsuario")); ?>
          <p>Elom Waizmam<?php //echo $nome[0] . ' ' . $nome[1] ?></p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>
    <!-- search form -->
    <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Buscar...">
            <span class="input-group-btn">
              <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
              </button>
            </span>
      </div>
    </form>
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">MENU DE NAVEGAÇÃO</li>
      <li>
        <a href="<?php echo base_url('Home')?>">
          <i class="fa fa-th"></i> <span>Home</span>
        </a>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-user"></i> <span>Dados Pessoais</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="active"><a href="<?php echo base_url('UsuariosController/viewPerfil')?>"><i class="fa fa-circle-o"></i> Meu Perfil</a></li>
          <li><a href="<?php echo base_url('UsuariosController/viewImpostoRenda')?>"><i class="fa fa-circle-o"></i> Imposto de Renda</a></li>
          <li><a href="<?php echo base_url('UsuariosController/viewBeneficios')?>"><i class="fa fa-circle-o"></i> Benefícios</a></li>
          <li><a href="<?php echo base_url('UsuariosController/viewContraCheque')?>"><i class="fa fa-circle-o"></i> Contracheques</a></li>
          <li><a href="index2.html"><i class="fa fa-circle-o"></i> Ficha Cooperado</a></li>
        </ul>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-recycle"></i> <span>A Coopas</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="active"><a href="<?php echo base_url('CoopasController/viewContatos')?>"><i class="fa fa-circle-o"></i> Contatos</a></li>
          <li><a href="index2.html"><i class="fa fa-circle-o"></i> Organograma</a></li>
          <li><a href="index2.html"><i class="fa fa-circle-o"></i> Cargos e Perfis</a></li>
          <li><a href="<?php echo base_url('CoopasController/viewCooperados')?>"><i class="fa fa-circle-o"></i> Cooperados</a></li>
          <li><a href="<?php echo base_url('CoopasController/viewColaboradores')?>"><i class="fa fa-circle-o"></i> Colaboradores</a></li>
        </ul>
      </li>
      <li>
        <a href="<?php echo base_url('Home')?>">
          <i class="fa fa-connectdevelop"></i> <span>Assessorias</span>
        </a>
      </li>      
      <li class="treeview">
        <a href="#">
          <i class="fa fa-balance-scale text-green"></i>
          <span>Legislação</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="pages/charts/chartjs.html"><i class="fa fa-circle-o text-blue"></i> LEI N.º 12.690/2012 </a></li>
          <li><a href="pages/charts/chartjs.html"><i class="fa fa-circle-o text-red"></i> LEI N.º 5.764 </a></li>
          <li><a href="pages/charts/chartjs.html"><i class="fa fa-circle-o text-green"></i> Estatuto COOPAS </a></li>
          <li><a href="pages/charts/chartjs.html"><i class="fa fa-circle-o text-yellow"></i> Regimento Interno </a></li>
        </ul>
      </li>      
      <li class="treeview">
        <a href="#">
          <i class="fa fa-users"></i>
          <span>Conselho Administrativo</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu"> 
          <li><a href="<?php echo base_url('ConselhoAdmin/viewMembrosAdmin')?>"><i class="fa fa-circle-o"></i> Membros do Conselho</a></li>
          <li><a href="<?php echo base_url('ConselhoAdmin/viewAtasAdmin')?>"><i class="fa fa-circle-o"></i> Atas</a></li>
          <li><a href="<?php echo base_url('ConselhoAdmin/viewReunioesAdmin')?>"><i class="fa fa-circle-o"></i> Reuniões do Conselho</a></li>
          <li><a href="pages/charts/flot.html"><i class="fa fa-circle-o"></i> Sugestões</a></li>
          <li><a href="pages/charts/inline.html"><i class="fa fa-circle-o"></i> Solicitação de Participação</a></li>
        </ul>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-users"></i>
          <span>Conselho Fiscal</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo base_url('ConselhoFiscal/membrosFiscal')?>"><i class="fa fa-circle-o"></i> Membros do Conselho</a></li>
          <li><a href="<?php echo base_url('ConselhoFiscal/atasFiscal')?>"><i class="fa fa-circle-o"></i> Atas</a></li>
          <li><a href="<?php echo base_url('ConselhoFiscal/viewReunioesFiscal')?>"><i class="fa fa-circle-o"></i> Reuniões do Conselho</a></li>
          <li><a href="pages/charts/flot.html"><i class="fa fa-circle-o"></i> Sugestões</a></li>
          <li><a href="pages/charts/inline.html"><i class="fa fa-circle-o"></i> Solicitação de Participação</a></li>
        </ul>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-pie-chart"></i>
          <span>Novos Projetos</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo base_url('NovosProjetos')?>"><i class="fa fa-circle-o"></i> Projetos em Andamento</a></li>
          <li><a href="pages/charts/morris.html"><i class="fa fa-circle-o"></i> Projetos Previstos</a></li>
          <li><a href="pages/charts/flot.html"><i class="fa fa-circle-o"></i> Sugestões de Projeto</a></li>
        </ul>
      </li>
      <li>
        <a href="<?php echo base_url('Cliente')?>">
          <i class="fa fa-trophy text-yellow"></i> <span>Clientes</span>
        </a>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-institution"></i>
          <span>Assembléias</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo base_url('AssembleiasController/viewCalendario')?>"><i class="fa fa-circle-o"></i> Calendário</a></li>
          <li><a href="<?php echo base_url('AssembleiasController/viewAtasPrestacaoContas')?>"><i class="fa fa-circle-o"></i>Atas Prestação de Contas</a></li>
          <li><a href="<?php echo base_url('AssembleiasController/viewOrcamentarias')?>"><i class="fa fa-circle-o"></i>Atas Orçamentária</a></li>
          <li><a href="<?php echo base_url('AssembleiasController/viewOrdinaria')?>"><i class="fa fa-circle-o"></i>Atas Ordinária</a></li>
        </ul>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-gavel"></i>
          <span>Comitê Eleitoral</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="pages/charts/chartjs.html"><i class="fa fa-circle-o"></i> Membros</a></li>
          <li><a href="<?php echo base_url('ComiteEleitoralController/viewCalendario')?>"><i class="fa fa-circle-o"></i> Calendário</a></li>
          <li><a href="pages/charts/flot.html"><i class="fa fa-circle-o"></i> Candidatos Atuais</a></li>
        </ul>
      </li>

      <li class="treeview">
        <a href="#">
        <i class="fa fa-money" aria-hidden="true"></i>
          <span>Orçamentos</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="pages/charts/chartjs.html"><i class="fa fa-circle-o"></i> Núcleo de TI </a></li>
          <li><a href="pages/charts/chartjs.html"><i class="fa fa-circle-o"></i> Núcleo de Engenharia </a></li>
          <li><a href="pages/charts/morris.html"><i class="fa fa-circle-o"></i> Núcleo de Produção</a></li>
          <li><a href="pages/charts/morris.html"><i class="fa fa-circle-o"></i> Núcleo de Jornalismo</a></li>
          <li><a href="pages/charts/morris.html"><i class="fa fa-circle-o"></i> Núcleo de Mídias</a></li>
          <li><a href="pages/charts/morris.html"><i class="fa fa-circle-o"></i> ASCOM</a></li>
          <li><a href="pages/charts/morris.html"><i class="fa fa-circle-o"></i> ADM</a></li>
        </ul>
      </li>

      <li class="treeview">
        <a href="#">
          <i class="fa fa-diamond"></i>
          <span>Fundos</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="pages/charts/chartjs.html"><i class="fa fa-circle-o"></i> Solidário </a></li>
          <li><a href="pages/charts/chartjs.html"><i class="fa fa-circle-o"></i> Bônus Social </a></li>
          <li><a href="pages/charts/morris.html"><i class="fa fa-circle-o"></i> sugestões</a></li>
        </ul>
      </li>

      <li>
        <a href="<?php echo base_url('Vagas')?>">
          <i class="fa fa-star-o text-blue"></i> <span>Vagas</span>
          <span class="pull-right-container">
            <small class="label pull-right bg-green">novo</small>
          </span>
        </a>
      </li>

    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
