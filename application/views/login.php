<div class="login-box">
<div class="login-logo">
  <a href="../../index2.html"><b>COOPAS</b> ADMIN</a>
</div>
<!-- /.login-logo -->
<div class="login-box-body">
  <?php $this->load->view('include/alertsMsg') ?>
  <p class="login-box-msg">Entre com usuário e senha para ter acesso ao sistema</p>

  <form action="<?php echo base_url('LoginIntranet/autentication') ?>" method="post">
    <div class="form-group has-feedback">
      <input name="email" type="email" class="form-control" placeholder="Email" required>
      <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
    </div>
    <div class="form-group has-feedback">
      <input name="senha" type="password" class="form-control" placeholder="Password" required>
      <span class="glyphicon glyphicon-lock form-control-feedback"></span>
    </div>
    <div class="row">
      <div class="col-xs-8">
        <div class="checkbox icheck">
          
        </div>
      </div>
      <!-- /.col -->
      <div class="col-xs-4">
        <button type="submit" class="btn btn-primary btn-block btn-flat">Entrar</button>
      </div>
      <!-- /.col -->
    </div>
  </form>


  <a href="#">Esqueci minha senha</a><br>
  <a href="<?php echo base_url('LoginIntranet/viewRegister') ?>" class="text-center">Cadastrar novo usuário</a>

</div>
<!-- /.login-box-body -->
</div>
<!-- /.login-box -->

