<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AssembleiasController extends CI_Controller {

	function __construct() {
		parent:: __construct();
	}

	public function viewCalendario(){
		$open['assetsBower'] = 'fullcalendar/dist/fullcalendar.min.css';
        $open['pluginCSS'] = '';
        $open['assetsCSS'] = '';
		$this->load->view('include/openDoc',$open);

		$this->load->view('paginas/assembleias/calendario');

		$footer['assetsJsBower'] = 'moment/min/moment.min.js,fullcalendar/dist/fullcalendar.min.js';
		$footer['pluginJS'] = '';
        $footer['assetsJs'] = 'assembleias/calendarios.js';
		$this->load->view('include/footer',$footer);
	}

	public function viewAtasPrestacaoContas(){
		$this->load->view('include/openDoc');
		$this->load->view('paginas/assembleias/atasPrestacaoContas');
		$this->load->view('include/footer');
	}

	public function viewOrcamentarias(){
		$this->load->view('include/openDoc');
		$this->load->view('paginas/assembleias/atasOrcamentarias');
		$this->load->view('include/footer');
	}

	public function viewOrdinaria(){
		$this->load->view('include/openDoc');
		$this->load->view('paginas/assembleias/atasOrdinaria');
		$this->load->view('include/footer');
	}


}
