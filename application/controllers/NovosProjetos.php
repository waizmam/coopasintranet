<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class NovosProjetos extends CI_Controller {

	function __construct() {
		parent:: __construct();
	}

	public function index(){
		$this->load->view('include/openDoc');
		$this->load->view('paginas/andamento');
		$this->load->view('include/footer');
	}


}
