<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cliente extends CI_Controller {

	function __construct() {
		parent:: __construct();
	}

	public function index(){
		$this->load->view('include/openDoc');
		$this->load->view('paginas/cliente');
		$this->load->view('include/footer');
	}


}
