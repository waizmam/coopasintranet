<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ConselhoAdmin extends CI_Controller {

	function __construct() {
		parent:: __construct();

		$this->load->model('adminDao_model');
	}

	public function viewMembrosAdmin(){
		$this->load->view('include/openDoc');
		$this->load->view('paginas/administrativo/membrosAdmin');
		$this->load->view('include/footer');
	}

	public function viewAtasAdmin(){
		$open['assetsBower'] = 'datatables.net-bs/css/dataTables.bootstrap.min.css';
		$open['pluginCSS'] = 'jqueryUi/jquery-ui.min.css';
        $open['assetsCSS'] = 'administrativo/atas-list.css';
		$this->load->view('include/openDoc',$open);

		$this->load->view('paginas/administrativo/atasAdmin');

		$footer['assetsJsBower'] = 'moment/min/moment.min.js,datatables.net/js/jquery.dataTables.min.js,datatables.net-bs/js/dataTables.bootstrap.min.js';
		$footer['pluginJS'] = 'jqueryUi/jquery-ui.min.js';
        $footer['assetsJs'] = 'administrativo/atasLista.js'; 
		$this->load->view('include/footer',$footer);
	}

	public function listaAtasDataTables(){
		
		$fetch_data = $this->adminDao_model->make_datatables_atas();
		

		$data = array();
		foreach($fetch_data as $row){         

			
			
			$dataDeCadastro = explode(' ', $row->dataCadastro );
			
			$sub_array = array();  
			$sub_array[] = converteDataInterface($row->data);  
			$sub_array[] = $row->tags;  
			$sub_array[] = $row->resumo;   
			$sub_array[] = converteDataInterface($dataDeCadastro[0]) . ' - ' . $dataDeCadastro[1];   
			$sub_array[] = $row->nome;   
			$sub_array[] = '<a href="'.base_url().'assets/atas/admin/'.$row->arquivo.'"> <img src="'.base_url().'assets/img/pdf.png"  class="img-thumbnail" width="70" height="110"></a>';
			$sub_array[] = '<a href="'.base_url('UsuariosController/viewAlterar/'.$row->idAtasAdmin).'" class="btn btn-app"><i class="fa fa-edit"></i> Alterar</a>
							<a href="'.base_url('UsuariosController/excluirUsuario/'.$row->idAtasAdmin).'" class="btn btn-app"><i class="fa fa-trash"></i> Excluir</a>';  
			
			$data[] = $sub_array;  
		}  
		$output = array(  
			"draw" => intval($_POST["draw"]),  
			"recordsTotal" => $this->adminDao_model->get_all_data_atas(),  
			"recordsFiltered" => $this->adminDao_model->get_filtered_data_atas(),  
			"data" => $data  
		);  
		echo json_encode($output);
	}

	public function viewReunioesAdmin(){

		$open['assetsBower'] = 'fullcalendar/dist/fullcalendar.min.css';
        $open['pluginCSS'] = '';
        $open['assetsCSS'] = 'checkbox_radio_styles.css,administrativo/calendarStyle.css';
		$this->load->view('include/openDoc',$open);

		$this->load->view('paginas/administrativo/reunioesAdmin');

		$footer['assetsJsBower'] = 'moment/min/moment.min.js,fullcalendar/dist/fullcalendar.min.js,bootstrap.dialog/dist/js/bootstrap-dialog.min.js';
		$footer['pluginJS'] = 'input-mask/jquery.inputmask.js';
        $footer['assetsJs'] = 'administrativo/calendarAdmin.js';
		$this->load->view('include/footer',$footer);
	}

	public function cadastrarReuniao(){

		$titulo = $this->input->post('titulo');
		$dataEvento = $this->input->post('dataDoEvento');
		$horaInicio = $this->input->post('horaInicio');
		$horaFim = $this->input->post('horaFim');
		$pauta = $this->input->post('pauta');
		$local = $this->input->post('local');
		$bgColor = $this->input->post('bgColor');

		$mensagem = array();

		if (empty($titulo)){
			$mensagem[] = '<b>TÍTULO</b> do evento é obrigatório.';
		}

		if (empty($horaInicio)){
			$mensagem[] = '<b>HORA INICIAL</b> do evento é obrigatório.';
		}

		if (empty($horaFim)){
			$mensagem[] = '<b>HORA FINAL</b> do evento é obrigatório.';
		}

		if (empty($local)){
			$mensagem[] = '<b>LOCAL</b> do evento é obrigatório.';
		}

		if (count($mensagem) > 0) {
			$retornoError = (array(
				'status' => 'error',
		  		'message' => $mensagem
			));
			echo json_encode($retorno);			
        }
        else{

			$data['idReuniaoAdmin'] = null;
			$data['titulo'] = $titulo;
			$data['data'] = $dataEvento;
			$data['horaInicio'] = $horaInicio;
			$data['horaFim'] = $horaFim;
			$data['pauta'] = $pauta;
			$data['local'] = $local;			
			$data['usuario_responsavel'] = $this->session->userdata("idUsuario");
			$data['bgColor'] = $bgColor;

			if($this->adminDao_model->insertReuniao($data)){
				$retorno = (array(
					'status' => TRUE,
					'message' => $mensagem
				));
				echo json_encode($retorno);			
			}else{
				$retorno = (array(
					'status' => FALSE,
					'message' => 'Erro ao inserir a nova reunião'
				));
				echo json_encode($retorno);			
			}			

		}		
		
	}


	public function reunioesAdminJSON(){
		
		$reunioes = $this->adminDao_model->selectAgendaAdmin();
		$arrayAgendaAdmin = null;
		foreach ($reunioes as $dados) {
			$arrayHoraInicio = explode(':',$dados->horaInicio);
			$horaInicio = $arrayHoraInicio[0].':'.$arrayHoraInicio[1];
			$arrayHoraFim = explode(':',$dados->horaFim);
			$horaFim = $arrayHoraFim[0].':'.$arrayHoraFim[1];

			$arrayAgendaAdmin[] = array(
				'id' => $dados->idReuniaoAdmin,
				'title' => $dados->titulo,				
				'start' => $dados->data."T".$dados->horaInicio,
				'end' => $dados->data."T".$dados->horaFim,
				'overlap' => '',
				'rendering' => '',
				'color' => '',	
				'url' => '',
				'backgroundColor'=> $dados->bgColor, 
				'borderColor'=> $dados->bgColor,
				'pauta'=>$dados->pauta,
				'local'=>$dados->local,
				'dataEvento'=> converteDataInterface($dados->data),
				'horaInicio'=>$horaInicio,
				'horaFim'=>$horaFim,				 				
			);
		}
		echo json_encode($arrayAgendaAdmin);			
	}

}
