<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vagas extends CI_Controller {

	function __construct() {
		parent:: __construct();
	}

	public function index(){
		$this->load->view('include/openDoc');
		$this->load->view('paginas/vagas');
		$this->load->view('include/footer');
	}


}
