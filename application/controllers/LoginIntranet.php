<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginIntranet extends CI_Controller {

	function __construct() {
		parent:: __construct();

		$this->load->helper('funcoes_helper');
		$this->load->model('usuarioDao_model');
		$this->load->model('loginDao_model');
	}

	public function index(){
		$this->load->view('include/openDoc');
		$this->load->view('login');	    
		$this->load->view('include/footer');
	}

	public function viewRegister(){
		
		$this->load->view('include/openDoc');
		
		$this->load->view('register');	   

		$footer['pluginJS'] = 'input-mask/jquery.inputmask.js';
        $footer['assetsJs'] = 'usuarios/usuarios-registrar.js'; 
        $this->load->view('include/footer',$footer);
	}

	public function cadatrarRegistro(){
	

		$nomeCompleto = xss_clean(sql_inject(trim($this->input->post('nomeCompleto'))));
		$email = xss_clean(sql_inject(trim($this->input->post('email'))));
		$senha = xss_clean(sql_inject(trim($this->input->post('senha'))));
		$cpf = xss_clean(sql_inject(trim($this->input->post('cpf'))));
		$dataNascimento = xss_clean(sql_inject(trim($this->input->post('dataNascimento'))));
		$tipoUsuario = xss_clean(sql_inject(trim($this->input->post('tipoUsuario'))));

		$mensagem = array();

		if(empty($nomeCompleto)){
			$mensagem[] = "O campo <b>Nome</b> é obrigatório!";			
		}

		if(empty($email)){
			$mensagem[] = "O campo <b>Email</b> é obrigatório!";			
		}else{
			if(!valid_email($email)){
				$mensagem[] = "<b>Email</b> Inválido!";				
		    }
		}

		if(empty($senha)){
			$mensagem[] = "O campo <b>Senha</b> é obrigatório!";			
		}

		if(empty($cpf)){
			$mensagem[] = "O campo <b>CPF</b> é obrigatório!";			
		}else if(!validar_cpf($cpf)){
			$mensagem[] = "Por favor, informe um <b>CPF</b> válido!";
		}

		if(ValidaData($dataNascimento) == 0){
			$mensagem[] = "Por favor, informe uma <b>Data de Nascimento</b> válida!";
		}

		if(count($mensagem)>0){
			$this->session->set_flashdata('mensagem',$mensagem);
			redirect(base_url() . 'LoginIntranet/viewRegister','refresh');
		}else{

			$user["idUsuario"] = null;
			$user["email"] = $email;
			$user["senha"] = md5($senha);
			$user["nome"] = $nomeCompleto;
			$user["cpf"] = tratarCPF($cpf);			
			$user["dataNascimento"] = converteDataBanco($dataNascimento);	
			$user["tipoUsuario"] = $tipoUsuario;		
			$user["status"] = 'ATIVO';

			if(!$this->usuarioDao_model->insertUsuario($user)){
				$this->session->set_flashdata('resultado_error','Erro ao cadastrar o usuário <b>'.$nomeCompleto.'</b>!');			
				redirect(base_url() . 'LoginIntranet/viewRegister','refresh');
			}else{
				$this->session->set_flashdata('resultado_ok','O Usuário <b>'.$nomeCompleto.'</b> foi cadastrado com sucesso!');			
				redirect(base_url() . 'LoginIntranet','refresh');
			}	
			
			
		}


	}


	public function autentication(){

		$email = sql_inject($this->input->post('email'));
		$senha = sql_inject($this->input->post('senha'));

		$mensagem = array();
		
		if(empty($email)){
			$mensagem[] = "Por favor, informe o <b>E-mail</b> para entrar";
		}
		if(empty($senha)){
			$mensagem[] = "Por favor, informe a <b>Senha</b> para entrar";
		}
		
		if(count($mensagem)>0){
			$this->session->set_flashdata ('mensagem',$mensagem);
			redirect(base_url().'LoginIntranet','refresh');
		}else{
			
			$data['email'] = strtolower($email);
			$data['senha'] = md5($senha);
			
			$loginUser = $this->loginDao_model->loginUser($data);
			if($loginUser[0]->avatar == '' || $loginUser[0]->avatar == null){
				$avatar = 'avatar33.png';
			}else{
				$avatar = $loginUser[0]->avatar;
			}
			
			if(count($loginUser)>0){

				$grupo = explode(',',$loginUser[0]->grupo);
				if($loginUser[0]->tipoUsuario == 'COO'){
					$tipoUsuario = 'COOPERADO';
				}else if($loginUser[0]->tipoUsuario == 'COL'){
					$tipoUsuario = 'COLABORADOR';
				}

				$newdata = array (
					'nomeUsuario' => $loginUser[0]->nome,					
					'idUsuario' => $loginUser[0]->idUsuario,
					'email' => $loginUser[0]->email,
					'formacao' => $loginUser[0]->formacao,
					'grupo' => $grupos,
					'tipoUsuario' => $tipoUsuario,
					'avatar' => $avatar,						
					'logged_in' => TRUE 
				);					
				$this->session->set_userdata($newdata);							
				redirect(base_url().'Home','refresh');
				
			}else{
				$mensagem[] = 'Usuário e/ou Senha incorretos!';
				$this->session->set_flashdata ('mensagem', $mensagem);
				redirect(base_url().'LoginIntranet','refresh');
			}
			
		}		

	}

	function logout() {
		$this->session->sess_destroy();
		$this->index();
	}


}
