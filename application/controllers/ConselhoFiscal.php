<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ConselhoFiscal extends CI_Controller {

	function __construct() {
		parent:: __construct();
	}

	public function membrosFiscal(){
		$this->load->view('include/openDoc');
		$this->load->view('paginas/fiscal/membrosFiscal');
		$this->load->view('include/footer');
	}

	public function atasFiscal(){
		$this->load->view('include/openDoc');
		$this->load->view('paginas/fiscal/atasFiscal');
		$this->load->view('include/footer');
	}

	public function viewReunioesFiscal(){
		
		$open['assetsBower'] = 'fullcalendar/dist/fullcalendar.min.css';
		$open['pluginCSS'] = '';
		$open['assetsCSS'] = '';
		$this->load->view('include/openDoc',$open);

		$this->load->view('paginas/fiscal/reunioesFiscal');

		$footer['assetsJsBower'] = 'moment/min/moment.min.js,fullcalendar/dist/fullcalendar.min.js';
		$footer['pluginJS'] = '';
		$footer['assetsJs'] = 'fiscal/calendarFiscal.js';
		$this->load->view('include/footer',$footer);
	}


}
