<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CoopasController extends CI_Controller {

	function __construct() {
		parent:: __construct();
	}

	public function viewContatos(){
		$this->load->view('include/openDoc');
		$this->load->view('paginas/coopas/contatos');
		$this->load->view('include/footer');
	}

	public function viewCooperados(){
		$this->load->view('include/openDoc');
		$this->load->view('paginas/coopas/cooperados');
		$this->load->view('include/footer');
	}

	public function viewColaboradores(){
		$this->load->view('include/openDoc');
		$this->load->view('paginas/coopas/colaboradores');
		$this->load->view('include/footer');
	}


}
