<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UsuariosController extends CI_Controller {

	function __construct() {
		parent:: __construct();
	}

	public function viewPerfil(){
		$open['assetsBower'] = '';
		$open['assetsCSS'] = 'perfil/perfil-avatar.css';
		$this->load->view('include/openDoc',$open);

		$this->load->view('paginas/usuarios/perfil');

		$footer['assetsJsBower'] = '';
		$footer['pluginJS'] = 'input-mask/jquery.inputmask.js';
		$footer['assetsJs'] = 'perfil/perfil.js';
		$this->load->view('include/footer',$footer);
	}


	public function viewImpostoRenda(){
		$open['assetsBower'] = 'datatables.net-bs/css/dataTables.bootstrap.min.css';
		$open['assetsCSS'] = 'perfil/perfil-impostoRenda.css';
		$this->load->view('include/openDoc',$open);

		$this->load->view('paginas/usuarios/impostoRenda');

		$footer['assetsJsBower'] = 'datatables.net/js/jquery.dataTables.min.js,datatables.net-bs/js/dataTables.bootstrap.min.js,';
		$footer['pluginJS'] = '';
		$footer['assetsJs'] = 'perfil/perfil.js';
		$this->load->view('include/footer',$footer);
	}

	public function viewContraCheque(){
		$open['assetsBower'] = 'datatables.net-bs/css/dataTables.bootstrap.min.css';
		$open['assetsCSS'] = 'perfil/perfil-impostoRenda.css';
		$this->load->view('include/openDoc',$open);

		$this->load->view('paginas/usuarios/contraCheque');

		$footer['assetsJsBower'] = 'datatables.net/js/jquery.dataTables.min.js,datatables.net-bs/js/dataTables.bootstrap.min.js,';
		$footer['pluginJS'] = '';
		$footer['assetsJs'] = 'perfil/perfil.js';
		$this->load->view('include/footer',$footer);
	}

	public function viewBeneficios(){
		$open['assetsBower'] = '';
		$open['assetsCSS'] = 'perfil/perfil-impostoRenda.css';
		$this->load->view('include/openDoc',$open);

		$this->load->view('paginas/usuarios/beneficios');

		$footer['assetsJsBower'] = '';
		$footer['pluginJS'] = '';
		$footer['assetsJs'] = 'perfil/impostoRenda.js';
		$this->load->view('include/footer',$footer);
	}



	


}
