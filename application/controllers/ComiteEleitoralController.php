<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ComiteEleitoralController extends CI_Controller {

	function __construct() {
		parent:: __construct();
	}

	public function viewCalendario(){
		$open['assetsBower'] = 'fullcalendar/dist/fullcalendar.min.css';
        $open['pluginCSS'] = '';
        $open['assetsCSS'] = '';
		$this->load->view('include/openDoc',$open);

		$this->load->view('paginas/comiteEleitoral/calendario');

		$footer['assetsJsBower'] = 'moment/min/moment.min.js,fullcalendar/dist/fullcalendar.min.js';
		$footer['pluginJS'] = '';
        $footer['assetsJs'] = 'comiteEleitoral/calendario.js';
		$this->load->view('include/footer',$footer);
	}


}
